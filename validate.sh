#!/bin/bash

case $DISK_TYPE in
    nvme | ssd) ;;
    *) echo "ERROR: DISK_TYPE must be either 'nvme' or 'ssd' not $DISK_TYPE" && ERROR=1;;
esac

case $CPU in
    amd | intel) ;;
    *) echo "ERROR: CPU must be either 'amd' or 'intel' not $CPU" && ERROR=2;;
esac

case $DESKTOP in
    gnome | plasma) ;;
    *) echo "ERROR: DESKTOP must be either 'plasma' or 'gnome' not $DESKTOP" && ERROR=3;;
esac

if [[ ERROR -gt 0 ]]
then
    echo "Script halted due to error in validation. No operations were performed."
    exit $ERROR
fi
