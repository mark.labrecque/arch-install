#!/bin/bash

source ./vars.sh
source ./validate.sh

pacman -Sy --noconfirm reflector
reflector -c Canada -c "United States" -a 6 --sort rate --save /etc/pacman.d/mirrorlist

pacman -S --noconfirm linux-headers git vim openssh networkmanager wpa_supplicant wireless_tools netctl dialog cups bash-completion rsync bat docker
sudo groupadd docker
sudo systemctl enable docker.service
sudo systemctl enable containerd.service

if [ $CPU == 'amd' ]
then
    pacman -S --noconfirm amd-ucode
elif [ $CPU == 'intel' ]
then
    pacman -S --noconfirm intel-ucode
fi

systemctl enable sshd
systemctl enable NetworkManager
systemctl enable cups.service
systemctl enable reflector.timer
systemctl enable bluetooth

# set timezones
localectl set-locale LANG=en_US.UTF-8
echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen
locale-gen

# user setup
echo "You now need to set the password on the ROOT user"
passwd
useradd -m -g users -G wheel,docker $USERNAME
echo "You now need to set the password on your new user, $USERNAME"
passwd $USERNAME

pacman -S --noconfirm sudo
sed -i "s/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/" /etc/sudoers

pacman -S --noconfirm grub efibootmgr dosfstools os-prober mtools
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
grub-mkconfig -o /boot/grub/grub.cfg

timedatectl set-timezone America/Vancouver
systemctl enable systemd-timesyncd
hostnamectl set-hostname $HOSTNAME

echo '127.0.0.1 localhost' >> /etc/hosts
echo '::1 localhost' >> /etc/hosts
echo '127.0.1.1 $HOSTNAME.localdomain $HOSTNAME' >> /etc/hosts

pacman -S --noconfirm xorg-server wayland xorg-xwayland

sed -i 's/MODULES=()/MODULES=(btrfs)/' /etc/mkinitcpio.conf
mkinitcpio -p linux

if [ $DESKTOP == 'gnome' ]
then
    sudo pacman -S --noconfirm gnome gnome-tweaks gnome-backgrounds
    sudo systemctl enable gdm
elif [ $DESKTOP == 'plasma' ]
then
    sudo pacman -S --noconfirm plasma-meta kde-applications packagekit-qt5 appstream
    sudo systemctl enable sddm
fi

mkdir /home/$USERNAME/setup
cp user_setup.sh /home/$USERNAME/setup.sh
cp vars.sh /home/$USERNAME/vars.sh
cp validate.sh /home/$USERNAME/validate.sh

echo "Once the reboot happens, you will want to run the user setup script to finish up."

echo "Alright, we should be ready to do a freestanding boot. Proceed with the following steps:"
echo " - exit"
echo " - umount --all"
echo " - reboot"