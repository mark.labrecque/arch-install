USERNAME='mark'
PASSWORD='password'
GIT_USERNAME='Mark Labrecque'
GIT_EMAIL='mark@affinitybridge.com'
ROOT_PASSWORD='password'
HOSTNAME='arch'

DISK='/dev/sda'
# DISK_TYPE
# - ssd
# - nvme
DISK_TYPE='ssd'

# CPU
# - amd
# - intel
CPU='amd'

# DESKTOP
# - plasma
# - gnome
DESKTOP='gnome'