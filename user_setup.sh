#!/bin/bash

source ./vars.sh
source ./validate.sh

# Install Rust
echo "Installing Rust..."
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

cd /tmp
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si

paru firefox flatpak timeshift timeshift-autosnap zramd
sudo systemctl enable --now zramd

curl -LO https://raw.githubusercontent.com/drud/ddev/master/scripts/install_ddev.sh && bash install_ddev.sh

flatpak install slack bitwarden -y

# Setup Git configuration
git config --global alias.st status
git config --global alias.br branch
git config --global alias.co checkout
git config --global user.name $GIT_USERNAME
git config --global user.email $GIT_EMAIL

mkdir /home/$USERNAME/Projects
mkdir /home/$USERNAME/Applications