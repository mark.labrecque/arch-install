#!/bin/bash

source ./vars.sh
source ./validate.sh

if [ $DISK_TYPE == 'nvme' ]
then
    BOOT_PART=$DISK'p1'
    HOME_PART=$DISK'p2'
elif [ $DISK_TYPE == 'ssd' ]
then
    BOOT_PART=$DISK'1'
    HOME_PART=$DISK'2'
else
    echo "ERROR: DISK_TYPE is set to an unexpected type: $DISK_TYPE"
    exit
fi

# Install disk prep packages
pacman -Sy --noconfirm gptfdisk

# Prep partions
sgdisk -Z ${DISK} # zap all on disk
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment

# create partitions
sgdisk -n 1:0:+350M ${DISK} # partition 1 (UEFI SYS), default start block, 300MB
sgdisk -n 2:0:0     ${DISK} # partition 3 (Root), default start, remaining

# set partition types
sgdisk -t 1:ef00 ${DISK}
sgdisk -t 2:8300 ${DISK}

# label partitions
sgdisk -c 1:"UEFI" ${DISK}
sgdisk -c 2:"ROOT" ${DISK}

# make filesystems
mkfs.vfat $BOOT_PART
mkfs.btrfs -f $HOME_PART

# Setup subvolumes
mount $HOME_PART /mnt
cd /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@var
cd /
umount /mnt

# Setup mounts
mount -o noatime,compress=zstd,ssd,discard=async,space_cache=v2,subvol=@ $HOME_PART /mnt
mkdir -p /mnt/{boot/efi,etc,home,var}
mount -o noatime,compress=zstd,ssd,discard=async,space_cache=v2,subvol=@home $HOME_PART /mnt/home
mount -o noatime,compress=zstd,ssd,discard=async,space_cache=v2,subvol=@var $HOME_PART /mnt/var
mount $BOOT_PART /mnt/boot/efi

# Generate fstab
genfstab -U /mnt >> /mnt/etc/fstab

# Install basic packages
pacstrap /mnt base base-devel linux btrfs-progs

# Copy over setup scripts
mkdir /mnt/arch-install
cp /root/arch-install/* /mnt/arch-install/

echo "First step is done, once the screen clears you need to run 'cd arch-install && sh install.sh'"
echo "Hit <Enter> to continue"
read CONFIRM

arch-chroot /mnt