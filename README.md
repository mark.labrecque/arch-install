## Arch Install - Public Profile

# Installation instructions
1) Confirm all of the vars defined in vars.sh before proceeding.
1) Run prepare.sh, following any prompts or instructions.
1) Run install.sh, following any prompts or instructions.
1) Reboot and profit.

# Known issues
The auto-partitioning is causing an issue with mismatched partition UUIDs, which prevents booting, but you can manually match them up by hand editing /etc/fstab against what blkid reads out.
